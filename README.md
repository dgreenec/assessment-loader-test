## Get Started

### run `yarn` or `npm install`

then

### `yarn start` or `npm run start`

Note: you must run this app under a Pearson hostname, e.g. http://local-dev.pearson.com:3000/, rather than http://localhost:3000/.

The IES session.js will throw the error "Sender: localhost, is not an allowable message origin."
