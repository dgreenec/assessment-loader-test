import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";

const bootstrap = async () => {
  const root = ReactDOM.createRoot(document.getElementById("root"));
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
};

const verifySessionAvailable = () => {
  if (window?.piSession?.getToken) {
    window?.piSession?.getToken((result, token) => {
      if (result === window?.piSession?.Success && token) {
        bootstrap();
      }
      // Token not present, force login
      window?.piSession?.login(window.location.href, 60, () => {});
    });
  } else {
    setTimeout(verifySessionAvailable, 50);
  }
};
verifySessionAvailable();
