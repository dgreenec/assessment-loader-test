import Assessment from "@assessments/loader/Assessment";
import AssessmentProvider from "@assessments/loader/AssessmentProvider";
import { IntlProvider } from "react-intl";
import "./App.css";

function App() {
  const assignmentId =
    "urn:pearson:assignment:8e5898f7-072c-420d-b6d6-cbb575ed20c8";
  const assessmentId =
    "urn:xl-mod-dev:assessment:Homework:6b482db7-e960-4108-8b8d-c16936586481";
  const courseId = "66101920e5c7317280904a52";

  const hostname = window?.location?.hostname;
  const userId = window?.piSession?.userId();

  return (
    <div className="App">
      {hostname === "localhost" && (
        <>
          <h1 style={{ color: "red" }}>
            Note: you must run this app under a pearson hostname, e.g.
            http://local-dev.pearson.com:3000/, rather than
            http://localhost:3000/.
          </h1>
          <h2>
            The IES session.js will throw the error "Sender: localhost, is not
            an allowable message origin."
          </h2>
        </>
      )}
      {userId && (
        <IntlProvider locale="en" defaultLocale="en">
          <AssessmentProvider
            baseUrl="https://tdx.acsdev.pearsondev.tech"
            // baseUrl="https://tdx-qa.acs.pearsondev.tech"
            user={userId}
          >
            <Assessment
              id={assessmentId}
              context={courseId}
              assessmentType="tdxAssessmentItem"
              policyUrl={`https://services-dev.seadev.pearsondev.tech/abs-api/sources/xl-mod-dev/learningcontexts/${courseId}/assessments/${assessmentId}/users/${userId}/policy?assignmentId=${assignmentId}`}
            />
          </AssessmentProvider>
        </IntlProvider>
      )}
    </div>
  );
}

export default App;
